﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace NadekoBot.Common
{
    public class NadekoRandom : Random
    {
        readonly RandomNumberGenerator _rng;

        public NadekoRandom() : base()
        {
            _rng = RandomNumberGenerator.Create();
        }

        public override int Next()
        {
            int result = default;

            Span<int> resultSpan = MemoryMarshal.CreateSpan(ref result, 1);
            _rng.GetBytes(MemoryMarshal.AsBytes(resultSpan));

            return Math.Abs(result);
        }

        public override int Next(int maxValue)
        {
            if (maxValue <= 0)
                throw new ArgumentOutOfRangeException(nameof(maxValue));

            return Next() % maxValue;
        }

        public override int Next(int minValue, int maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException(nameof(maxValue));

            if (minValue == maxValue)
                return minValue;


            return Next() % (maxValue - minValue) + minValue;
        }

        public long NextLong(long minValue, long maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException(nameof(maxValue));

            if (minValue == maxValue)
                return minValue;

            return Next() % (maxValue - minValue) + minValue;
        }

        public override void NextBytes(byte[] buffer)
        {
            _rng.GetBytes(buffer);
        }

        protected override double Sample()
        {
            double result = default;

            Span<double> resultSpan = MemoryMarshal.CreateSpan(ref result, 1);
            _rng.GetBytes(MemoryMarshal.AsBytes(resultSpan));

            return Math.Abs(result / double.MaxValue + 1);
        }

        public override double NextDouble()
        {
            double result = default;

            Span<double> resultSpan = MemoryMarshal.CreateSpan(ref result, 1);
            _rng.GetBytes(MemoryMarshal.AsBytes(resultSpan));

            return result;
        }
    }
}
